const BigInteger = require('jsbn').BigInteger;

function checkBigIntegers(...potentialIntegers) 
{    
    for(var i = 0; i < potentialIntegers.length; i++)
    {
        if(!(potentialIntegers[i] instanceof BigInteger))
        {
            throw new TypeError('Not an instance of BigInteger');
        }
    }
}

function checkType(value, type)
{
    if(!(value instanceof type))
    {
        throw new TypeError('Incorrect type');
    }
}

function allEqual() {
        var len = arguments.length;
        for (var i = 1; i< len; i++){
           if (arguments[i] === null || arguments[i] !== arguments[i-1])
              return false;
        }
        return true;
}
module.exports = {
    checkBigIntegers: checkBigIntegers,
    checkType: checkType,
    allEqual: allEqual
}