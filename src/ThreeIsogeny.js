const MontgomeryCurve = require('./MontgomeryCurve.js');
const F2elm = require('./F2elm.js');
const F2Point = require('./F2Point.js');

class ThreeIsogeny extends MontgomeryCurve {
    constructor(curve) {
        this._a = curve._a;
        this._b = curve._b;
        this._c = curve._c;
        this._update();
    }
    get3Isog(p) {
        var px = p.getX();
        var pz = p.getY();

        var t0 = px.square();
        var t1 = t0.add(t0);
        t0 = t0.add(t1);
        t1 = pz.square();
        this._a = t1.square();
        t1 = t1.add(t1);
        this._c = t1.add(t1);
        t1 = t0.subtract(t1);
        t1 = t0.multiply(t1);
        this._a = this._a.subtract(t1);
        this._a = this._a.subtract(t1);
        this._a = this._a.subtract(t1);
        t1 = px.multiply(pz);
        this._c = this._c.multiply(t1);

        this._update();
    }

    eval3Isog(p, q) {
        var px = p.getX();
        var pz = p.getZ();

        var qx = q.getX();
        var qz = q.getZ();

        var t0 = px.multiply(qx);
        var t1 = pz.multiply(qx);
        var t2 = pz.multiply(qz);
        t0 = t0.subtract(t2);
        t2 = px.multiply(qz);
        t1 = t1.subtract(t2);
        t0 = t0.square();
        t1 = t1.square();
        qx = qx.multiply(t0);
        qz = qz.multiply(t1);

        return new F2Point(qx, qz);
    }
}

module.exports = ThreeIsogeny;
