const BigInteger = require('jsbn').BigInteger;
const F2elm = require('./F2elm.js');
const Felm = require('./Felm.js');
const F2Point = require('./F2Point.js');
const PointProjective = require('./PointProjective.js');
const utils = require('./utils.js');

class MontgomeryCurve {
    constructor(a, b, c) {
        utils.checkType(a, F2elm);
        utils.checkType(b, F2elm);
        utils.checkType(c, F2elm);
        if(!utils.allEqual(a.getPrime(), b.getPrime(), c.getPrime()))
        {
            throw new Error('Cannot create montgomery curve with elements from different prime fields');
        }
        this._a = a;
        this._b = b;
        this._c = c;
        this._update();
    }
    _update() {
        if(this._a.isEven())
        {
            this._a24 = this._c.add(a.div2());
            this._c24 = this._c.add(this._c);
        }
        else
        {
            this._c24 = this._c.add(this._c);
            this._a24 = this._c24.add(this._a);
            this._c24 = this._c24.add(this._c24);
        }
    }
    static distortAndDiff(px) {
        var xd1 = px.square();
        xd1 = xd1.add(Felm.ONE);
        var zd0 = px.add(px);

        return new F2Point(Felm.ZERO, xd1, zd0, Felm.ZERO);
    }

    xDblAddBasefield(p, q, xpq) {
        var res = [];

        var fpC24 = this._c24.get0();
        var fpA24 = this._a24.get0();

        var qx = q.getX();
        var qz = q.getZ();

        var px = p.getX();
        var pz = p.getZ();

        var t0 = px.add(pz);
        var t1 = px.subtract(pz);
        px = t0.square();
        var t2 = qx.subtract(qz);
        qx = qx.add(qz);
        t0 = t0.multiply(t2);
        pz = t1.square();
        t1 = t1.multiply(qx);
        t2 = px.subtract(pz);
        t2 = t2.multiply(fpA24);

        pz = pz.multiply(fpC24);
        px = px.multiply(pz);
        pz = t2.add(pz);

        qz = t0.subtract(t1);
        qx = t0.add(t1);
        pz = pz.multiply(t2);

        qz = qz.square();
        qx = qx.square();
        qz = qz.multiply(xpq);

        res[0] = new PointProjective(px, pz);
        res[1] = new PointProjective(qx, qz);

        return res;
    }

    static xAdd(p, q, xpq) {
        var px = p.getX();
        var pz = p.getZ();

        var qx = q.getX();
        var qz = q.getZ();
        
        var t0 = px.add(pz);
        var t1 = px.subtract(pz);
        px = qx.subtract(qz);
        pz = qx.add(qz);
        t0 = t0.multiply(px);
        t1 = t1.multiply(pz);
        pz = t0.subtract(t1);
        px = t0.add(t1);
        pz = pz.square();
        px = px.square();
        pz = pz.multiply(xpq);

        return new F2Point(px, pz);
    }

    xDbl(p) {
        var px = p.getX();
        var pz = p.getZ();

        var t0 = px.subtract(pz);
        var t1 = px.add(pz);
        t0 = t0.square();
        t1 = t1.square();
        var qz = this._c24.multiply(t0);
        var qx = t1.multiply(qz);
        t1 = t1.subtract(t0);
        t0 = this._a24.multiply(t1);
        qz = qz.add(t0);
        qz = qz.multiply(t1);

        return new F2Point(qx, qz);
    }
    xDble(p, e) {
        var q = p;
        for(var i = 0; i < e; i++) {
            q = this.xDbl(q);
        }

        return q;
    }
    xTpl(p) {
        var x = p.getX();
        var z = p.getZ();

        var t2 = x.add(z);
        var t0 = x.square();
        var t1 = z.square();
        t2 = t2.square();
        var t3 = t0.multiply(this._c);
        t2 = t2.subtract(t0);
        var t4 = t1.multiply(this._c);
        t2 = t2.subtract(t1);
        var t5 = t3.add(t4);
        t2 = t2.multiply(this._a);
        t3 = t3.add(t3);
        t4 = t4.add(t4);
        t3 = t3.add(t2);
        t4 = t4.add(t2);
        t3 = t3.add(t5);
        t4 = t4.add(t5);
        t2 = t0.subtract(t1);
        t0 = t0.add(t0);
        t1 = t1.add(t1);
        t2 = t2.multiply(t5);
        t1 = t1.multiply(t3);
        t0 = t0.multiply(t4);
        t1 = t1.subtract(t2);

        t0 = t0.add(t2);

        t1 = t1.square();

        t0 = t0.square();

        t3 = x.multiply(t1);

        t4 = z.multiply(t0);

        return F2Point(t3, t4);

    }
    xTple(p, e) {
        var q = p;
        for(var i = 0; i < e; i++)
        {
            q = this.xTpl(q);
        }
    }

    xDblAdd(p, q, xpq) {
        var pq = [];

        var px = p.getX();
        var pz = p.getZ();
        var qx = q.getX();
        var qz = q.getZ();

        var t0 = px.add(pz);
        var t1 = px.subtract(pz);
        px = t0.square();
        var t2 = qx.subtract(qz);
        qx = qx.add(qz);
        t0 = t0.multiply(t2);
        pz = t1.square();
        t1 = t1.multiply(qx);
        var t3 = px.subtract(pz);

        t2 = t3.multiply(this._a24);
        pz = pz.multiply(this._c24);
        px = px.multiply(pz);
        pz = t2.add(pz);

        qz = t0.subtract(t1);
        qx = t0.add(t1);
        pz = t2.add(pz);

        qz = qz.square();
        qx = qx.square();
        qz = qz.multiply(xpq);

        pq[0] = new F2Point(px, pz);
        pq[1] = new F2Point(qx, qz);
        
        return pq;
    }
    ladder(x, m, obits) {
        var pq = [];

        pq[0] = new PointProjective(PointProjective.INFINITY);
        pq[1] = new PointProjective(x, Felm.ONE);

        var nbits = m.bitLength();

        for(var i = obits; i > nbits; i--)
        {
            pq[1] = pq[0].swapPointsBasefield(pq[1], BigInteger.ZERO);
            pq = this.xDblAddBasefield(pq[0], pq[1], x);
            pq[1] = pq[0].swapPointsBasefield(pq[1], BigInteger.ZERO);
        }

        var bit;
        for(var i = nbits - 1; i >=0; i--)
        {
            bit = m.testBit(i) ? BigInteger.ONE : BigInteger.ZERO;

            pq[1] = pq[0].swapPointsBasefield(pq[1], bit);
            pq = this.xDblAddBasefield(pq[0], pq[1], x);
            pq[1] = pq[0].swapPointsBasefield(pq[1], bit);
        }

        return pq;
    }
    ladder3pt(px, qx, xpq, m, obits) {
        var uv = [];
        uv[0] = F2Point.INFINITY;
        uv[1] = new F2Point(qx, F2elm.ONE);
        var w = new F2Point(px, F2elm.ONE);

        var nbits = m.bitLength();
        var bit = BigInteger.ZERO;

        for(var i = obits; i > nbits; i--)
        {
            uv[0] = w.swapPoints(uv[0], bit);
            uv[1] = uv[0].swapPoints(uv[1], bit);
            var temp1 = px.select(qx, bit);
            var temp2 = qx.select(xpq, bit);
            w = MontgomeryCurve.xAdd(w, uv[0], temp1);
            uv = MontgomeryCurve.xDblAdd(uv[0], uv[1], temp2);
            uv[1] = uv[0].swapPoints(uv[1], bit);
            uv[0] = w.swapPoints(uv[0], bit);

        }

        for(var i = nbits - 1; i >= 0; i--) {
            bit = m.testBit(i) ? BigInteger.ONE : BigInteger.ZERO;
            
            uv[0] = w.swapPoints(uv[0], bit);
            uv[1] = uv[0].swapPoints(uv[1], bit);
            var temp1 = px.select(qx, bit);
            var temp2 = qx.select(xpq, bit);
            w = MontgomeryCurve.xAdd(w, uv[0], temp1);
            uv = MontgomeryCurve.xDblAdd(uv[0], uv[1], temp2);
            uv[1] = uv[0].swapPoints(uv[1], bit);
            uv[0] = w.swapPoints(uv[0], bit);
        }

        return w;
    }
    jInv() {
        var jinv = this._a.square();
        var t1 = this._c.square();
        var t0 = t1.add(t1);
        t0 = jinv.subtract(t0);
        t0 = t0.subtract(t1);
        jinv = t0.subtract(t1);
        t1 = t1.square();
        jinv = jinv.multiply(t1);
        t0 = t0.add(t0);
        t0 = t0.add(t0);
        t1 = t0.square();
        t0 = t0.multiply(t1);
        t0 = t0.add(t0);
        t0 = t0.add(t0);
        jinv = jinv.inverse();
        jinv = t0.multiply(jinv);

        return jinv;
    }
    toString() {
        return this._b + ' y^2 = ' + this._c + ' x^3 + ' + this._a + ' x^2 + ' + this._c + ' x  where (a24/c24) = (' + this._a24 + '/' + ')';
    }
}

module.exports = MontgomeryCurve;