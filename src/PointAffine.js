const Felm = require('./Felm.js');

class PointAffine {
    static fromInts(xc, yc, prime) {
        return new PointAffine(new Felm(xc, prime), new Felm(xy, prime));
    }
    static fromPointAffine(pa) {
        return new PointAffine(pa._x, pa._y);
    }
    constructor(xc, yc) {
        if(xc._p != yc._p){
            throw new Error('Cannot create point affine with field elements from different prime fields');
        }
        this._x = xc;
        this._y = yc;
    }
    getX() {
        return this._x;
    }
    getY() {
        return this._y;
    }
    swapPointsBasefield(q, option) {
        var qx = this._x.swap(q._x, option);
        var qy = this._y.swap(q._y, option);
        return new PointAffine(qx, qy);
    }
    toString() {
        return '(' + x + ', ' + y + ')';
    }
}

module.exports = PointAffine;